<?php

namespace Veribrand\API;

class Base
{
  use \Bricks\Hookable;

  static $actions = [
    'rest_api_init' => 'register_basic_route',
    'woocommerce_rest_check_permissions' => ['woocommerce_allow_read', 10, 4],
    'rest_api_init' => 'modify_jwt_data'
  ];

  public static function init()
  {
    static::set_hooks();
  }

  static function get_basic_info()
  {
    $main_menu = wp_get_nav_menu_items('Principal');

    return [
      'main_menu' => $main_menu
    ];
  }

  static function register_basic_route()
  {
    $class = get_called_class();
    register_rest_route('veri/v1', '/basic', [
      'methods' => 'GET',
      'callback' => [$class, 'get_basic_info']
    ]);
  }

  static function modify_jwt_data()
  {
    add_filter('jwt_auth_token_before_dispatch', function ($data) {
      $user = get_user_by('email', $data['user_email']);
      $data['user_id'] = $user->ID;
      return $data;
    }, 10, 1);

    add_filter('jwt_auth_token_before_sign', function ($token) {
      $user = get_user_by('ID', $token['data']['user']['id']);
      if ($user->user_login === 'readonly') {
        $token['exp'] = time() + YEAR_IN_SECONDS * 40;
      }
      return $token;
    }, 10, 1);
  }

  static function woocommerce_allow_read($permission, $context, $object_id, $post_type)
  {
    if ($context !== 'read') {
      return $permission;
    }

    $post_type_object = get_post_type_object($post_type);

    if ($object_id) {
      return current_user_can($post_type_object->cap->read_post, $object_id);
    }

    return $post_type_object->has_archive;
  }
}
