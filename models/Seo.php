<?php

  namespace Veribrand;

  class Seo {
    use \Bricks\Hookable;

    static $actions = [
      'rest_api_init' => 'register_seo_post_fields'
    ];

    static function register_seo_post_fields() {
      register_rest_field('post', [

      ]);
    }
  }
